FROM ubuntu:18.04
COPY "./ares-cli" "/ares-cli"
WORKDIR "/ares-cli/bin"
CMD "/ares-cli/bin/ares"