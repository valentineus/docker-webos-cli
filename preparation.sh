#!/bin/bash
# Author:       "Valentin Popov" <info@valentineus.link>
# Email:        info@valentineus.link
# Date:         2020-02-18
# Usage:        /bin/bash preparation.sh
# Description:  Preparation the "ares" package.

# Updating the Environment
PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
export PATH="$PATH:/usr/local/scripts"

# Download archive
wget https://webosose.s3.ap-northeast-2.amazonaws.com/tools/cli/v1.10.2/ares-webos-cli-linux-1.10.2.tgz

# Unpack archive
tar -zxvf ./ares-webos-cli-linux-1.10.2.tgz

# End of work
exit 0
